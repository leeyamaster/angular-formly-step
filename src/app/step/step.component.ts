import {Component, ViewChild} from '@angular/core';
import { Step2Component } from '../step2/step2.component';
import {NzNotificationService} from "ng-zorro-antd/notification";
import {Form, FormControl, FormGroup, Validators} from "@angular/forms";
@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.less']
})
export class StepComponent {
  @ViewChild(Step2Component) step2Component: Step2Component | undefined

  constructor(private notification: NzNotificationService) {
  }
  current = 0;
  pre(): void {
    this.current -= 1;
  }

  next(): void {
    // console.log(this.step2Component?.form.value)
    // console.log(this.step2Component?.form.valid)
    let current = this.current
    const getForm = () => {
      if(current === 0) return this.step2Component?.form.get('form1') as FormGroup
      if(current === 1) return this.step2Component?.form.get('form2') as FormGroup
      return null
    }
    console.log(getForm())
    // @ts-ignore
    Object.values(getForm()?.controls).forEach(control => {
      if (control.invalid) {
        control.markAsDirty();
        control.updateValueAndValidity({ onlySelf: true });
      }
    });
    // console.log(getForm()?.controls)
    getForm()?.markAllAsTouched()
    if(getForm()?.valid){
      this.current += 1;
    }else{
      this.notification
        .error(
          'Notification',
          'Form must be filled out'
        )
        .onClick.subscribe(() => { });
    }
  }

  done(): void {
    console.log('done');
  }

  cancel(){

  }
  validateForm:FormGroup = new FormGroup({
    selectdData: new FormControl('',Validators.required)
  });
  submitForm(){
    // this.validateForm.markAsDirty()
    // this.validateForm.updateValueAndValidity({ onlySelf: true });
    // Object.values(this.validateForm.controls).forEach(control => {
    //   if (control.invalid) {
    //     control.markAsDirty();
    //     control.updateValueAndValidity({ onlySelf: true });
    //   }
    // });
    // this.validateForm.markAllAsTouched();
    // console.log(this.validateForm.valid)
  }
}
