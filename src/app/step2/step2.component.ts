import {Component, Input, SimpleChanges} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {FormlyFieldConfig, FormlyFormOptions} from '@ngx-formly/core';
import {province1, checkOptionsOne, category, product} from './info'

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.less']
})
export class Step2Component {
  @Input() current: number = 0;
  form = new FormGroup({
    current: new FormControl(0),
    form1: new FormGroup({

    }),
    form2: new FormGroup({})
  });
  model1: any = {};
  model2: any = {};
  fields1: FormlyFieldConfig[] = [

    // step 1
    {
      key: 'firstName',
      type: 'input',
      defaultValue: '',
      className: 'col-6',
      modelOptions: {
        debounce: {
          default: 500,
        },
        updateOn: 'change',
      },
      templateOptions: {
        label: '请输入您的姓',
        required: true
      },
      validation: {
        messages: {
          required: '该项必填'
        },
      },
      expressions: {},
    },
    {
      key: 'lastName',
      type: 'input',
      defaultValue: '',
      templateOptions: {
        label: '请输入您的名',
        required: true
      },
      validation: {
        messages: {
          required: '该项必填'
        },
      },
      expressions: {},
    },
    {
      key: 'fullName',
      type: 'input',
      defaultValue: '',
      templateOptions: {
        label: '您的名字',
        required: true
      },
      validation: {
        messages: {
          required: '该项必填'
        },
      },
      expressions: {
        'templateOptions.disabled': 'true',
        'model.fullName': 'model.firstName + "" + model.lastName',
      }
    },
    {
      key: 'gender',
      type: 'radio',
      templateOptions: {
        options: [
          {label: '男', value: 'male'},
          {label: '女', value: 'female'},
        ],
        required: true
      },
      validation: {
        messages: {
          required: '该项必填'
        },
      },
      expressions: {}
    },
    {
      key: 'hasHouse',
      type: 'checkbox',
      defaultValue: false,
      templateOptions: {
        label: '您有房吗?',
      },
      expressions: {},
    },
    // todo Select require is not finished
    {
      key: 'category',
      type: 'my-select',
      templateOptions: {
        label: '您想要哪一类产品',
        required: true,
      },
      validation: {
        required: true,
        messages: {
          required: '该项必填'
        },
      },
      props: {
        options: category
      },
      expressions: {},
    },
    {
      key: 'product',
      type: 'select',
      templateOptions: {
        label: '请选择您的商品：',
        required: true
      },
      validation: {
        messages: {
          required: '该项必填'
        },
      },
      props: {
        options: []
      },
      expressions: {
        'props.options': (field) => {
          const products = product
          const category = field.model.category;
          if (category) return products.filter(item => item.key === category).map(v => ({
            label: v.label,
            value: v.value
          }))
          return []
        },
        'props.disabled': '!model.category',
      }
    },]
  fields2: FormlyFieldConfig[] = [
    // step2
    {
      key: 'selectKey3',
      type: 'select',
      templateOptions: {
        label: '分组选择',
        options: [
          {label: '钢铁侠', value: 1001, group: '男'},
          {label: '美国队长', value: 1002, group: '男'},
          {label: '黑寡妇', value: 1003, group: '女'},
          {label: '浩克', value: 1004, group: '男'},
          {label: '女巫', value: 1005, group: '女'},
        ],
      },
      expressions: {
      },
    },
    {
      key: 'selectKey4',
      type: 'select',
      templateOptions: {
        label: '配置属性的分组选择',
        options: [
          {name: '钢铁侠', id: 1001, gender: '男'},
          {name: '美国队长', id: 1002, gender: '男'},
          {name: '黑寡妇', id: 1003, gender: '女'},
          {name: '浩克', id: 1004, gender: '男'},
          {name: '女巫', id: 1005, gender: '女'},
        ],
        groupProp: 'gender',
        valueProp: 'id',
        labelProp: 'name',
      },
      expressions: {
      },
    },
    {
      key: 'fruits',
      type: 'checkbox-group',
      templateOptions: {
        label: 'Fruits',
        options: checkOptionsOne,
        required: true
      },
      validation: {
        messages: {
          required: '该项必填'
        },
      },
    },
    {
      key: 'text',
      type: 'textarea',
      templateOptions: {
        label: 'textarea',
        options: []
      },
      expressions: {
      },
    },
    {
      key: 'selection',
      type: 'radio',
      props: {
        description: 'Description',
        required: true,
        label: 'Checkbox',
        options: [
          {label: 'Checkbox 1', value: 'checkbox1'},
          {label: 'Checkbox 2', value: 'checkbox2'},
          {label: 'Checkbox 3', value: 'checkbox3'}
        ],
      },
      // 添加触发事件
      hooks: {
        onInit: (field) => {
          console.log(field)
          field.formControl?.valueChanges.subscribe(value => {
            // 判断选择的值，显示或隐藏字段组
            const d = this.fields2.find(item => item.key === 'checkboxes')
            console.log('d', d, value)
            if (value === 'option2') {
              if (d) d.hide = false; // 显示字段组中的第二个字段
            } else {
              if (d) d.hide = true; // 隐藏字段组中的第二个字段
            }
          });
        }
      },
      expressions: {
      },
    },
    {
      key: 'checkboxes',
      type: 'select',
      props: {
        description: 'Description',
        required: true,
        multiple: true,
        label: 'Checkbox Selection',
        options: [
          {label: 'Checkbox 1', value: 'checkbox1'},
          {label: 'Checkbox 2', value: 'checkbox2'},
          {label: 'Checkbox 3', value: 'checkbox3'}
        ],
      },
      templateOptions: {},
      expressions: {
      },
      // hide: true // 初始时隐藏字段
    },
    {
      key: 'date',
      type: 'date-picker',
      expressions: {
      },
    },
    {
      key: 'cascader',
      type: 'cascader',
      templateOptions: {
        label: 'Cascader',
        options: province1,
        change: (field, event) => {
          console.log(field)
          // 处理选择变化的逻辑
        },
      },
      expressions: {
        hide: '!(model.current === 2)',
      },
    },
  ];


  onSubmit() {
    console.log(this.model1)
  }

  test() {
    console.log(this.form.valid);
  }

  getForm(str: 'form1'| 'form2') {
    return this.form.get(str) as FormGroup
  }

  ngOnInit() {
    setTimeout(() => {
      this.form.patchValue({current: this.current});
      this.form.get('category')?.setValidators(Validators.required);
      this.form.get('category')?.updateValueAndValidity();
    }, 10)
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.form.patchValue(
      {current: changes['current'].currentValue}
    )
  }
}
