export const checkOptionsOne = [
  {label: 'Apple', value: 'Apple', checked: true},
  {label: 'Pear', value: 'Pear', checked: false},
  {label: 'Orange', value: 'Orange', checked: false}
];
export const province1 = [
  {
    value: 'fujian',
    label: 'Fujian',
    children: [
      {
        value: 'xiamen',
        label: 'Xiamen',
        children: [
          {
            value: 'Kulangsu',
            label: 'Kulangsu',
            isLeaf: true
          }
        ]
      }
    ]
  },
  {
    value: 'guangxi',
    label: 'Guangxi',
    children: [
      {
        value: 'guilin',
        label: 'Guilin',
        children: [
          {
            value: 'Lijiang',
            label: 'Li Jiang River',
            isLeaf: true
          }
        ]
      }
    ]
  }
];
export const category = [
  {label: "电子产品", value: "electronic"},
  {label: "家居用品", value: "house"},
  {label: "服装与饰品", value: "clothing"},
  {label: "运动与户外", value: "outdoors"},
  {label: "家用电器", value: "electrical"}
]

export const product = [
  {label: "手机", value: "phone", key: 'electronic'},
  {label: "平板电脑", value: "ipad", key: 'electronic'},
  {label: "电视", value: "TV", key: 'electronic'},
  {label: "音响", value: "sound", key: 'electronic'},
  {label: "相机", value: "camera", key: 'electronic'},
  {label: "家具", value: "furniture", key: 'house'},
  {label: "家装", value: "homeDecoration", key: 'house'},
  {label: "厨具", value: "kitchen", key: 'house'},
  {label: "卫浴用品", value: "bathroom", key: 'house'},
  {label: "男装", value: "camera", key: 'clothing'},
  {label: "女装", value: "camera", key: 'clothing'},
  {label: "鞋子", value: "camera", key: 'clothing'},
  {label: "配饰", value: "camera", key: 'clothing'},
  {label: "运动装备", value: "camera", key: 'outdoors'},
  {label: "户外用品", value: "camera", key: 'outdoors'},
  {label: "洗衣机", value: "camera", key: 'electrical'},
  {label: "冰箱", value: "camera", key: 'electrical'},
  {label: "空调", value: "camera", key: 'electrical'},
  {label: "吸尘器", value: "camera", key: 'electrical'},
]
