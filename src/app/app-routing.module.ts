import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Step2Component} from "./step2/step2.component";
import {StepComponent} from "./step/step.component";

const routes: Routes = [
  {path: 'index', component: StepComponent},
  {path: 'step2', component: Step2Component},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
