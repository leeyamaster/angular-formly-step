import {Component} from '@angular/core';
import {FieldType, FieldTypeConfig} from '@ngx-formly/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-checkbox-group',
  template: `
    <nz-checkbox-group
      [formControl]="formControl"
      [ngModel]="to.options"
      (ngModelChange)="to.change && to.change(field, $event)">
    </nz-checkbox-group>
  `,
})
export class MyCheckboxGroupComponent extends FieldType<FieldTypeConfig> {

}
