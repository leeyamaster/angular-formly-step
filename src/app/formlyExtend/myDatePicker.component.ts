import {Component} from '@angular/core';
import {FieldType, FieldTypeConfig} from '@ngx-formly/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-checkbox-group',
  template: `
    <nz-date-picker
      [formControl]="formControl"
      [nzOptions]="to.options"
      (ngModelChange)="to.change && to.change(field, $event)"></nz-date-picker>
  `,
})
export class MyDatePickerComponent extends FieldType<FieldTypeConfig> {

}
