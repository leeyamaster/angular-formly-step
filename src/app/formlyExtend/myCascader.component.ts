import {Component} from '@angular/core';
import {FieldType, FieldTypeConfig} from '@ngx-formly/core';
import {NzCascaderOption} from "ng-zorro-antd/cascader";
import {Observable} from "rxjs";
@Component({
  selector: 'app-cascader',
  template: `
    <nz-cascader
      [nzOptions]="convert(to.options)"
      [formControl]="formControl"
      (ngModelChange)="to.change && to.change(field, $event)"></nz-cascader>
  `,
})
export class MyCascaderComponent extends FieldType<FieldTypeConfig> {
  // to: { options: NzCascaderOption[] | Observable<NzCascaderOption[]> | undefined, change?: any };
  constructor() {
    super();
  }
  convert(options: any[]| Observable<any[]> | undefined):NzCascaderOption[] | null{
    if (options instanceof Observable) {
      // 订阅 Observable 并返回结果
      // return options.subscribe(val => val);
      return null;
    } else if (Array.isArray(options)) {
      // 如果 options 是数组，直接返回
      return options;
    } else {
      // 如果 options 是 undefined，返回 null
      return null;
    }
  }
}
