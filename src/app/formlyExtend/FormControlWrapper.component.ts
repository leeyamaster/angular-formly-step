import { Component } from '@angular/core';
import {FieldType, FieldTypeConfig, FieldWrapper} from "@ngx-formly/core";

@Component({
  selector: 'app-form-control-wrapper',
  template: `
    <nz-form-item>
         <nz-form-control nzErrorTip="Please select your gender!" nzRequired>
            <ng-container #fieldComponent></ng-container>
         </nz-form-control>
    </nz-form-item>
`,
  styles: [
    `

    `
  ]
})
export class FormControlWrapperComponent extends FieldWrapper {
}
