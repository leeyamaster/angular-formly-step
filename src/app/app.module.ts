import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule  } from '@angular/common/http';
import { BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { MyNgZorroAntdModule } from './ng-zorro-antd.module'
import { AppRoutingModule } from "./app-routing.module";
import {LetModule, PushModule} from '@ngrx/component';
import {StepComponent} from "./step/step.component";
import {Step2Component} from "./step2/step2.component";
import {FormlyModule} from "@ngx-formly/core";
import {FormlyNgZorroAntdModule} from "@ngx-formly/ng-zorro-antd";
import {MyCheckboxGroupComponent} from "./formlyExtend/myCheckboxGroup.component";
import {MyDatePickerComponent} from "./formlyExtend/myDatePicker.component";
import { MyCascaderComponent } from './formlyExtend/myCascader.component';
import { FormControlWrapperComponent } from './formlyExtend/FormControlWrapper.component';
import {MySelectComponent} from "./formlyExtend/mySelect.component";

registerLocaleData(en);

// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    StepComponent,
    Step2Component,
    MyCheckboxGroupComponent,
    MyDatePickerComponent,
    MyCascaderComponent,
    FormControlWrapperComponent,
    MySelectComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
      MyNgZorroAntdModule,
    AppRoutingModule,
    NoopAnimationsModule,
    LetModule,
    PushModule,
    FormlyModule.forRoot({
      types: [
        { name: 'checkbox-group', component: MyCheckboxGroupComponent },
        { name: 'date-picker', component: MyDatePickerComponent },
        { name: 'cascader', component: MyCascaderComponent },
        { name: 'my-select', component: MySelectComponent }
      ],
      wrappers: [
        { name: 'form-control', component: FormControlWrapperComponent }
      ]
    }),
    FormlyNgZorroAntdModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
