# 学习资料

1. formly v6 官网： https://formly.dev/docs/guide/getting-started
2. formly v8 官网： http://docs.angular-formly.com/
3. formly github 含demo： https://github.com/ngx-formly/ngx 
4. formly 中文实例： https://blog.csdn.net/guizi0809/article/details/119782637
5. formly 中文实例2： https://blog.csdn.net/guizi0809/article/details/119795456
6. B站中文学习视频： https://www.bilibili.com/video/BV1aP411e7kV/


### 个人建议
我上手这框架的时候，擦坑特别多，首先，gitee，github上的代码，都特别少，包括chatgpt都很少示例，甚至我踩坑，chatgpt告诉我的代码都是错误的，并且在使用这框架的时候，会遇到的一些坑：
1. form里设置hide属性，里面的表达式，如果表达式里的值不是form表单里的数据，那么只会初始化执行一次，并不会改变表达式里的值后再自动执行，也不能写方法把代码抽象出来，只能写字符串表达式。
2. 扩展性差，例如Select的Require属性。
3. step表单，当我进入下一步，再返回时，数据全部消失。`  expressions: {},
      hideExpression: (model) => ...
      hide: true`这三行代码，一二行只会监听Form表单里改变的值，外部的值不会监听，`hide: xxx`只会在初始化时，才会调用。怎么解决step表单这个问题？我是分割为多个Form表单，通过*ngIf指令来进行隐藏，这样我感觉是最合理的方式。
4. export class MyDatePickerComponent extends FieldType<FieldTypeConfig> {}    这里必须必须加<FieldTypeConfig>
5. 检测不到Select，Checkbox的required，这是个大问题，总不能另外写一个表单引入Select吧
总之Angular的Formly框架不建议上手，实际开发中更会遇到问题。最好写个demo，试一下可行性

##### 好的地方：
适用于小型表单，必须小规模交互，以及没有特殊情况（表格，文件，时间控件等等的情况），只有小规模Input框，这时可以使用这框架。

##### 未完成的地方：
目前是Select Required属性还未实现。有想法的，可以实现下，用了不少办法了。